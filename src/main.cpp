#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Caracter.hpp"
#include "Map.hpp"

#define LARGEUR_FENETRE 600
#define HAUTEUR_FENETRE 600
#define VITESSE 3

void refresh( SDL_Renderer*, SDL_Rect);
void chargeMenu(SDL_Renderer*);

int main() {
    // Variables utiles
    bool swMap=false; // Swap la grille
    bool stop = false; // Non fin
    int menu = 1;
    SDL_Event event; // Event SDL
    SDL_Renderer *renderer; // Pointeur rendu
    Map map("../ressources/grille1.txt");

    //
    SDL_Rect coordinateRenderer = {0, 0, LARGEUR_FENETRE, HAUTEUR_FENETRE};

    // Gestion du temps
    Uint32 last_time = SDL_GetTicks();
    Uint32 current_time, ellapsed_time;
    Uint32 start_time; // Gestion des FPS

    // Creation du perso, Jim
    Caracter Jim(map.getxPop(), map.getyPop(), "../ressources/jim.png");

    // Création de la fenêtre
    SDL_Window *screen = SDL_CreateWindow("ReblochonJAM",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          LARGEUR_FENETRE, HAUTEUR_FENETRE,
                                          SDL_WINDOW_SHOWN);
    // Création du rendu
    renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    SDL_RenderFillRect(renderer, &coordinateRenderer);


    // Boucle evenementielle
    while (!stop) {
        switch (menu) {
            case 1 :
                chargeMenu(renderer);
                SDL_RenderPresent(renderer);
                SDL_RenderClear(renderer);

                SDL_WaitEvent(&event);
                switch (event.type) {
                    case SDL_MOUSEBUTTONDOWN:
                        if(event.button.y>0.66*HAUTEUR_FENETRE &&
                                event.button.y < 0.8*HAUTEUR_FENETRE &&
                                event.button.x<0.33*LARGEUR_FENETRE &&
                                event.button.x>0.1*LARGEUR_FENETRE)
                            std::cout<<event.button.x<<" "<<event.button.y<<std::endl;
                            menu = 2;
                        break;

                    case SDL_QUIT:
                        stop = true;
                        break;
                }
                break;

            case 2 : // Jeu en cours
                SDL_Delay(50);
				stop = map.fin(Jim.posY_,Jim.posX_);
                while (SDL_PollEvent(&event) && !stop) {
                    // Event QUIT
                    if (event.window.event == SDL_WINDOWEVENT_CLOSE) {
                        stop = true;
                        system("killall xeyes");
                    }
                    // Event APPUIE TOUCHE
                    if (event.type == SDL_KEYDOWN) {
                        // Touche Gauche ou Q
                        if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_LEFT] ||
                            SDL_GetKeyboardState(NULL)[SDL_SCANCODE_Q]) {
                            Jim.avancer(-VITESSE, 0, LARGEUR_FENETRE, HAUTEUR_FENETRE,map);
                        }
                        // Touche Droite ou D
                        if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_RIGHT] ||
                            SDL_GetKeyboardState(NULL)[SDL_SCANCODE_D]) {
                            Jim.avancer(VITESSE, 0, LARGEUR_FENETRE, HAUTEUR_FENETRE,map);
                        }
                        // Touche Haut ou Z
                        if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_UP] || SDL_GetKeyboardState(NULL)[SDL_SCANCODE_Z]) {
                            Jim.avancer(0, -VITESSE, LARGEUR_FENETRE, HAUTEUR_FENETRE,map);
                        }
                        // Touche Bas ou S
                        if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_DOWN] ||
                            SDL_GetKeyboardState(NULL)[SDL_SCANCODE_S]) {
                            Jim.avancer(0, VITESSE, LARGEUR_FENETRE, HAUTEUR_FENETRE,map);
                        }

                    }

                }

                // Calcul du temps écoulé
                current_time = SDL_GetTicks();
                ellapsed_time = current_time - last_time;
                if(ellapsed_time>20000) {
                    last_time=current_time;
                    swMap=true;
                }

                // Switch d'état régulièrement fonction du temps
                if (SDL_GetTicks() % 70 == 0) Jim.switchSwitch();
                if(swMap){
                    map.switchGrille();
                    swMap=false;
                }

                // Refresh le renderer | clean entre autre
                refresh(renderer, coordinateRenderer);

                // Charge le renderer avec les textures du joueurs et du niveau (dans cet ordre)
                map.afficher(renderer);
                Jim.afficher(renderer);

                // Blit le renderer sur l'écran
                SDL_RenderPresent(renderer);
                break;
        }
    }


    // Destroy | vide mémoire
    SDL_DestroyWindow(screen);
    SDL_Quit();
    return 0;

}

void refresh(SDL_Renderer* renderer, SDL_Rect coordinates){
    // Clear le renderer pour vider la map actuelle
    SDL_RenderClear(renderer);
    // Charge un fond blanc
    //SDL_SetRenderDrawColor(renderer,255,255,255,0);
    SDL_RenderFillRect(renderer,&coordinates);
}

void chargeMenu(SDL_Renderer* renderer){
    // Init variables
    SDL_Texture* image=NULL;
    SDL_Rect coordinates{0,0,LARGEUR_FENETRE,HAUTEUR_FENETRE};
    // Charge la surface avec l'image correspondate
    SDL_Surface * surface=IMG_Load("../ressources/menu.png");
    // Charge la texture via la surface sur la texture
    image= SDL_CreateTextureFromSurface(renderer,surface);
    // Charge sur le renderer en position top
    SDL_RenderCopy(renderer,image,NULL,&coordinates);
    // Libère la texture
    SDL_DestroyTexture(image);
}
