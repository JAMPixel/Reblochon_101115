//
// Created by ju on 10/11/15.
//

#ifndef REBLOCHONJAM_MAP_H
#define REBLOCHONJAM_MAP_H

#include <iostream>
#include <vector>

class Map
{
	public :

	int largeur_;	
	int hauteur_;
	int xPop_;
	int yPop_;
	int tailleMur_;
    bool swap;
	std::string images_;
    std::string imagesEnd_;
	std::vector<std::vector<bool> > grille_;//1 -> mur ; 0 -> pas mur
	SDL_Texture * image_;
    SDL_Texture * imageEnd_;
	SDL_Surface * surface_;
	SDL_Surface * surfaceEnd_;
	int xFin_;
	int yFin_;
	
	public :
	
	Map();
	Map(int largeur, int hauteur);
	Map(std::string const & nomFic);
	int getHauteur();
	int getLargeur();
	void setPop(int x, int y);
	bool collision(int x, int y);
	void addMur(int x, int y);
	void suprMur(int x, int y);
	void afficher(SDL_Renderer* renderer);
    void switchGrille();
	int getxPop();
	int getyPop();
	int getTailleMur();
	bool fin(int x, int y);
};

#endif //REBLOCHONJAM_MAP_H
