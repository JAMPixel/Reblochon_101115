//
// Created by ju on 10/11/15.
//

#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include "Map.hpp"

Map::Map() : largeur_(12), hauteur_(12), swap(false) ,xPop_(1), yPop_(1), tailleMur_(20), images_("../ressources/mur.png"), grille_(largeur_,std::vector<bool>(hauteur_,0)), image_(0),  surface_(IMG_Load(images_.c_str())), xFin_(10), yFin_(10)
{
	
}

Map::Map(int largeur, int hauteur) : largeur_(largeur),swap(false) , hauteur_(hauteur), xPop_(1), yPop_(1), tailleMur_(20), images_("../ressources/mur.png"), grille_(largeur_,std::vector<bool>(hauteur_,0)), image_(0),  surface_(IMG_Load(images_.c_str())), xFin_(10), yFin_(10)
{
	
}

Map::Map(std::string const & nomFic) : tailleMur_(40), swap(false) ,images_("../ressources/mur.png"), imagesEnd_("../ressources/stargate.jpg") , imageEnd_(0), image_(0), surfaceEnd_(IMG_Load(imagesEnd_.c_str())),  surface_(IMG_Load(images_.c_str()))
{
	std::ifstream ficMap(nomFic, std::ios::in);
	
	if (ficMap)
	{
		ficMap >> largeur_;
		ficMap >> hauteur_;
		ficMap >> xFin_;
		ficMap >> yFin_;
		grille_ = std::vector<std::vector<bool> >(largeur_, std::vector<bool>(hauteur_));
		
		xPop_ = 1;
		yPop_ = 1;
		
		char val;
		
		for (int i = 0; i<largeur_; i++)
		{
			for (int j = 0; j<hauteur_; j++)
			{
				ficMap >> val;
				grille_[i][j] = atoi(&val);
			}
		}
		
		ficMap.close();
	}
}

int Map::getHauteur()
{
	return hauteur_;
}

int Map::getLargeur()
{
	return largeur_;
}

void Map::setPop(int x, int y)
{
	xPop_ = x;
	yPop_ = y;
}

bool Map::collision(int x, int y)
{
	return (!(grille_[y/tailleMur_][x/tailleMur_]));
}

void Map::addMur(int x, int y)
{
	grille_[x][y] = 1;
}

void Map::suprMur(int x, int y)
{
	grille_[x][y] = 0;
}

void Map::afficher(SDL_Renderer* renderer)
{
	image_ = SDL_CreateTextureFromSurface(renderer, surface_);
	// Affectation texture de chacun des blocs
    for(int i = 0; i<largeur_;i++)
    {
    	for (int j = 0; j<hauteur_;j++)
    	{
            if(!swap){
                if(grille_[i][j])
                {
                    SDL_Rect coordinates{j*tailleMur_,i*tailleMur_,tailleMur_,tailleMur_};
                    SDL_RenderCopy(renderer,image_,NULL,&coordinates);
                }
            }
    		else{
                if(!grille_[i][j])
                {
                    SDL_Rect coordinates{j*tailleMur_,i*tailleMur_,tailleMur_,tailleMur_};
                    SDL_RenderCopy(renderer,image_,NULL,&coordinates);
                }
            }
		}
	}
    // Changement d'affectation pour le bloc de fin
    imageEnd_=SDL_CreateTextureFromSurface(renderer, surfaceEnd_);
	SDL_Rect coordinates{xFin_*tailleMur_,yFin_*tailleMur_,tailleMur_,tailleMur_};
    SDL_RenderCopy(renderer,imageEnd_,NULL,&coordinates);

	SDL_DestroyTexture(image_);
}

int Map::getxPop()
{
	return xPop_*tailleMur_;
}

int Map::getyPop()
{
	return yPop_*tailleMur_;
}

int Map::getTailleMur()
{
	return tailleMur_;
}

bool Map::fin(int x, int y)
{
	return (xFin_*tailleMur_<x && x<xFin_*tailleMur_+(tailleMur_/2) && yFin_*tailleMur_<y && y<yFin_*tailleMur_+(tailleMur_/2));
}

void Map::switchGrille() {
    swap=true;
}