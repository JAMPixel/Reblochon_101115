//
// Created by maxime on 10/11/15.
//

#include "Caracter.hpp"

// Créateur par défaut
Caracter::Caracter(){
    posX_=0;
    posY_=0;
    switch_=true;
    taille_=20;
    image_="../ressource/jim.png";
}

// Créateur | prérequis : x0, y0, lien du sprite
Caracter::Caracter(int x, int y, std::string s) {
    posX_=x;
    posY_=y;
    switch_=true;
    taille_=20;
    image_=s;
}

void Caracter::swapTexture(std::string s) { //Useless
    image_=s;
}

void Caracter::avancer(int x, int y, int largeur, int hauteur, Map & map){ //TODO Ajouter les collisions via un passage en parametre de la matrice MAP
    if(switch_) { // Sans invertion
        if (x>0){
            if( posX_+taille_ != largeur && map.collision(posX_+taille_+x,posY_+2) && map.collision(posX_+taille_+x,posY_+taille_-2)) posX_+=x;
        }
        else{
            if( posX_ !=0  && map.collision(posX_+x,posY_+2) && map.collision(posX_+x,posY_+taille_-2)) posX_+=x;
        }
        if (y>0){
            if( posY_ + taille_ != hauteur && map.collision(posX_+2,posY_+taille_+y)  && map.collision(posX_+taille_-2,posY_+taille_+y)) posY_+=y;
        }
        else{
            if( posY_ != 0  && map.collision(posX_+2,posY_+y)  && map.collision(posX_+taille_-2,posY_+y)) posY_+=y;
        }
    }
    else{ // Avec invertion
        if(x>0){
            if( posX_ !=0  && map.collision(posX_-x,posY_+2) && map.collision(posX_-x,posY_+taille_-2)) posX_-=x;
        }
        else{
            if( posX_+taille_ != largeur && map.collision(posX_+taille_-x,posY_+2)  && map.collision(posX_+taille_-x,posY_+taille_-2)) posX_-=x;
        }
        if(y>0){
            if( posY_ != 0  && map.collision(posX_+2,posY_-y)  && map.collision(posX_+taille_-2,posY_-y)) posY_-=y;
        }
        else{
            if( posY_ + taille_ != hauteur && map.collision(posX_+2,posY_+taille_-y) && map.collision(posX_+taille_-2,posY_+taille_-y)) posY_-=y;
        }
    }
}

void Caracter::switchSwitch(){
    switch_=!switch_;
    if(!switch_) image_="../ressources/antijim.png";
    else image_="../ressources/jim.png";
}

void Caracter::afficher(SDL_Renderer* renderer){
    // Init variables
    SDL_Texture* image=NULL;
    SDL_Rect coordinates{posX_,posY_,taille_,taille_};
    // Charge la surface avec l'image correspondate
    SDL_Surface * surface=IMG_Load(image_.c_str());
    // Charge la texture via la surface sur la texture
    image= SDL_CreateTextureFromSurface(renderer,surface);
    // Charge sur le renderer en position top
    SDL_RenderCopy(renderer,image,NULL,&coordinates);
    // Libère la texture
    SDL_DestroyTexture(image);
}

Caracter::~Caracter(){

}
