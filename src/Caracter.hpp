//
// Created by maxime on 10/11/15.
//

#ifndef REBLOCHONJAM_CARACTER_H
#define REBLOCHONJAM_CARACTER_H

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include <string>
#include "Map.hpp"

class Caracter {
public :
    int posX_; // Position en x
    int posY_; // Position en y
    int taille_; // Taille du perso
    bool switch_; // Vrai : pas d'inversion | Faux : inversion
    std::string image_; // Chemin vers l'image

public:
    Caracter(); // Constructeur

    Caracter(int,int,std::string); // Constructeur utile

    void swapTexture(std::string); // Change le chemin de l'image

    void avancer(int,int,int,int, Map &); // Evolution des positons | Gère l'inversion

    void switchSwitch(); // Change la valeur de switch_

    void afficher(SDL_Renderer*);

    ~Caracter(); // Destructeur
};


#endif //REBLOCHONJAM_CARACTER_H
