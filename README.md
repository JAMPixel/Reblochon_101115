Roblochon
---

## Contexte

Ce jeux a été devellopé au cours de la JAM des 10 et 11 nombre 2015 du club Pixel de l'ISMA (Clermont-Ferrand).
Le principe est simple: 24h pour créer un jeux en partant seulement d'un thème/mot et dans le cas présent il s'agit de "Inversion".

## Le but du jeux

Roblochon est un petit jeux de labyrinthe où le fonctionnement des touche de déplacement change de manière aléatoire.

## Techno :

* c++ 11
* SDL2 et SDL2_Image

> sudo apt-get install libsdl2-dev libsdl2-image-dev